import * as Helper from './helper.js'

function SectionUploadsModule() {
    var self = this;

    this.apiHost = "https://api.camfindapp.com";

    this.url = {
        sendImage: this.apiHost + "/v1/public_posts"
    }

    this.isHomepage = window.document.body.classList.contains("js-homepage");
    this.isContactUs = window.document.body.classList.contains("js-contact");

    this.DOM = {};
    this.DOM.uploadsDrop = window.document.querySelector('.uploads');
    this.DOM.dragContainer = window.document.querySelector('.how-drag-info');
    this.DOM.uploadContainer = window.document.querySelector('.upload-container-js');
    this.DOM.dragImg = window.document.querySelector('.drag-js');
    this.DOM.dropArea = window.document.querySelector('.drop-area-js');
    this.DOM.uploadInput = window.document.querySelector('.uploads__input')

    this.DOM.tryAgainBtn = window.document.querySelector('.try-again-js')
    this.DOM.previewBlock = window.document.querySelector('.preview-js');
    this.DOM.imagePreview = window.document.querySelector('.image-preview-js');
    this.DOM.sectionUploadsBg = window.document.querySelector('.uploads-bg-js');
    this.DOM.resultContainer = window.document.querySelector('.result-container');
    this.DOM.resultImg = window.document.querySelector('.result-img-js');
    this.DOM.resultWrapper = window.document.querySelector('.result-wrapper');
    this.DOM.resultTitle = window.document.querySelector('.section-uploads__subhed');

    this.DOM.wormSvg = window.document.querySelector('svg.worm-loader__worm--progress path');
    this.DOM.loaderList = window.document.querySelector('.worm-loader__list');
    this.DOM.loaderStage1 = window.document.querySelector('.loader-stage-1-js');
    this.DOM.loaderStage2 = window.document.querySelector('.loader-stage-2-js');
    this.DOM.loaderStage3 = window.document.querySelector('.loader-stage-3-js');
    this.DOM.loaderStage4 = window.document.querySelector('.loader-stage-4-js');

    this.DOM.dragImg.addEventListener("dragstart", dragstart)
    this.DOM.dragImg.addEventListener("dragend", dragend)

    this.DOM.dropArea.addEventListener("dragover", dropZoneDragOver)
    this.DOM.dropArea.addEventListener("dragenter", dropZoneDragenter)
    this.DOM.dropArea.addEventListener("dragleave", dropZoneDragleave)
    this.DOM.dropArea.addEventListener("drop", dropZoneDrop)

    this.defaultPreviewImgPath = this.DOM.dragImg.getAttribute("data-img-big");
    this.imageBase64Preview = "";
    this.isUploading = false;
    this.animations = {};
    
    this.DOM.tryAgainBtn.addEventListener('click', (event) => {
        this.animations.tryAgain.progress(0)
        this.animations.tryAgain.play();
    })

    this.DOM.uploadInput.addEventListener('change', onFileChoose)

    function dragstart(e) {
        e.dataTransfer.setData("defaultPath", self.defaultPreviewImgPath)
        self.DOM.dragContainer.classList.add("how-drag-info--cover")
    }

    function dragend(e) {
        if (!self.isUploading) {
            self.DOM.dragContainer.classList.remove("how-drag-info--hide")
            self.DOM.dragContainer.classList.remove("how-drag-info--cover")
        }
    }

    function dropZoneDragOver(e) {
        e.preventDefault()
    }

    function dropZoneDragenter(e) {
        e.preventDefault()
        if (!self.isUploading) {
            this.classList.add("uploads--drag-enter");
        }
    }

    function dropZoneDragleave() {
        if (!self.isUploading) {
            this.classList.remove("uploads--drag-enter");
        }
    }

    function onFileChoose(event) {
        if (self.isUploading) {
            return;
        }

        event.preventDefault();
        
        self.DOM.dropArea.classList.remove("uploads--drag-enter");
        self.DOM.dropArea.classList.add("uploads--uploading");
        self.DOM.dragContainer.classList.add("how-drag-info--hide");
        self.DOM.dragContainer.classList.remove("how-drag-info--cover")
        self.DOM.sectionUploadsBg.classList.add("section-uploads__background--hide")
        self.animations.animateWorm.progress(0);
        self.animations.animateWorm.play();

        const file = self.DOM.uploadInput.files[0]
        let reader = new FileReader();
        reader.onload = (e) => {
            self.imageBase64Preview = e.target.result;
            self.DOM.resultImg.src = self.imageBase64Preview;
            self.DOM.imagePreview.src = self.imageBase64Preview;
            self.DOM.uploadInput.value = "";
        }
        reader.readAsDataURL(file);

        self.sendImage(file)
    }

    function dropZoneDrop(event) {
        event.preventDefault();

        if (self.isUploading) {
            return;
        }

        self.isUploading = true;
        let file;

        this.classList.remove("uploads--drag-enter");
        this.classList.add("uploads--uploading");
        self.DOM.dragContainer.classList.add("how-drag-info--hide");
        self.DOM.dragContainer.classList.remove("how-drag-info--cover")
        self.DOM.sectionUploadsBg.classList.add("section-uploads__background--hide")
        self.animations.animateWorm.progress(0);
        self.animations.animateWorm.play();

        if (event.dataTransfer.files[0] && event.dataTransfer.files[0].type.includes('image')) {
            file = event.dataTransfer.files[0]
            let reader = new FileReader();
            reader.onload = (e) => {
                self.imageBase64Preview = e.target.result;
                self.DOM.resultImg.src = self.imageBase64Preview;
                self.DOM.imagePreview.src = self.imageBase64Preview;
            }
            reader.readAsDataURL(file);
            self.sendImage(file)
        } else {
            const path = window.origin + event.dataTransfer.getData('defaultPath');
            Helper.toDataUrl(path, (base64) => {
                self.imageBase64Preview = base64;
                self.DOM.resultImg.src = self.imageBase64Preview;
                self.DOM.imagePreview.src = self.imageBase64Preview;

                file = Helper.dataURItoBlob(self.imageBase64Preview)
                self.sendImage(file)
            })
        }
    }
}

SectionUploadsModule.prototype.sendImage = function(imageFile) {
    let formData = new FormData();
    formData.append('post[image]', imageFile);

    var xhr = new XMLHttpRequest();
    xhr.open("POST",  this.url.sendImage, true);

    xhr.onreadystatechange = (event) => {
        if (xhr.readyState == 4) {
            
            if (xhr.status == 201 && xhr.response) {
                this.animations.animateWorm.progress(0.72);
                this.animations.animateWorm.paused(true);
                this.animations.animateWorm.play();
                
                const result = JSON.parse(xhr.response)
                this.DOM.resultTitle.innerHTML = result.search_result
            } else {
                this.animations.animateWorm.progress(0);
                this.animations.animateWorm.paused(true);
                this.animations.tryAgain.progress(0);
                this.animations.tryAgain.play();
    
                this.super.showContactModal("Fail")
            }
        }
    };
    xhr.send(formData);
}

SectionUploadsModule.prototype.setUpAnimations = function() {
    function getWormProgress(progress) {
        return `${(223 * progress) / 100}, 400`;
    }

    this.animations.animateWorm = new TimelineMax({
        paused: true,
        onComplete: () => {
            this.toggleResult = true; 
            this.isUploading = false; 
        }
    })
        .set(this.DOM.loaderList, { y: 0 })
        .set(this.DOM.previewBlock, {
            css: {
                className: "+=preview__image--fade"
            }
        })
        .set(this.DOM.wormSvg, {
            ease: Power0.easeNone,
            'stroke-dasharray': getWormProgress(0),
        })
        .to(this.DOM.loaderList, 0.4, {
            autoAlpha: 1
        }, 'initiation')
        .to(this.DOM.loaderList, 0.4, {
            y: "-=16px"
        }, 'initiation')
        .to(this.DOM.wormSvg, 22/25, {
            ease: Power0.easeNone,
            'stroke-dasharray': getWormProgress(22),
        }, 'initiation')
        .set(this.DOM.previewBlock, {
            css: {
              className: "+=preview__image--blinking"
            }
        })
        .to(this.DOM.loaderStage1, 0.4, {
          autoAlpha: 0
        }, 'stage1-2')
        .to(this.DOM.loaderList, 0.4, {
            y: "-=16px"
        }, 'stage1-2')
        .to(this.DOM.loaderStage2, 0.4, {
          autoAlpha: 1
        }, 'stage1-2')
        .to(this.DOM.wormSvg, 46/25, {
            ease: Power0.easeNone,
            'stroke-dasharray': getWormProgress(68)
        }, "stage2-3")
        .to(this.DOM.loaderStage2, 0.4, {
            autoAlpha: 0
        }, 'stage2-3')
        .to(this.DOM.loaderList, 0.4, {
            y: "-=16px"
        }, 'stage2-3')
        .to(this.DOM.loaderStage3, 0.4, {
            autoAlpha: 1
        }, 'stage2-3')
        .set(this.DOM.previewBlock, {
            css: {
                className: "-=preview__image--blinking"
            }
        })
        .addPause()
        .set(this.DOM.loaderStage1, { autoAlpha: 0 })
        .set(this.DOM.loaderStage2, { autoAlpha: 0 })
        .set(this.DOM.loaderStage3, { autoAlpha: 0 })
        .set(this.DOM.loaderList, { y: 0 })
        .set(this.DOM.loaderStage4, { y: "16px" })
        .to(this.DOM.wormSvg, 0.3, {
            ease: Power0.easeNone,
            'stroke-dasharray': getWormProgress(100)
        }, 'stage3-4')
        .to(this.DOM.loaderList, 0.4, {
            y: "-=16px"
        }, 'stage3-4')
        .to(this.DOM.loaderStage4, 0.4, {
            autoAlpha: 1, y: 0
        }, 'stage3-4')
        .set(this.DOM.resultContainer, {
            y: "-70px"
        })
        .to(this.DOM.dropArea, 0.4, { scale: 0.2 }, "same")
        .set(this.DOM.dropArea, { autoAlpha: 0 })
        .to(this.DOM.uploadContainer, 0.4, { y: 0 }, "same")
        .set(this.DOM.resultWrapper, {
            css: { className: "-=result-wrapper--hide" }
        })
        .set(this.DOM.resultContainer, {
            css: {  display: "block" },
        })
        .to(this.DOM.resultContainer, 0.5, {
            y: "0"
        }, "show result")

    this.animations.tryAgain = new TimelineMax({
        paused: true,
        onComplete: () => {
            this.isUploading = false;
        }
    })
        .set(this.DOM.sectionUploadsBg, {
            css: { className: "-=section-uploads__background--hide" }
        })
        .set(this.DOM.resultContainer, {
            css: {  display: "none" },
        })
        .set(this.DOM.resultWrapper, {
            css: { className: "+=result-wrapper--hide" }
        })
        .set(this.DOM.resultContainer, {
            css: {  display: "none" },
        })
        .set(this.DOM.dragContainer, {
            css: { className: "-=how-drag-info--hide" }
        }, "sameTime")
        .set(this.DOM.dropArea, {
            css: { className: "-=uploads--uploading" }
        }, "sameTime")
        .to(this.DOM.resultContainer, 0.5, {
            y: "-70px"
        }, "sameTime")
        .set(this.DOM.dropArea, { scale: 1 }, "sameTime")
        .to(this.DOM.dropArea, 0.4, {
            autoAlpha: 1
        }, "sameTime")
};

SectionUploadsModule.prototype.init = function(parent) {
    this.super = parent;

    this.setUpAnimations();
};

export default SectionUploadsModule;
