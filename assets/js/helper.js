const keys = { 18: 1, 37: 1, 38: 1, 39: 1, 40: 1, 32: 1, 33: 1, 34: 1, 35: 1, 36: 1 };

export function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault) {
		e.preventDefault();
	}
	e.returnValue = false;
};

export function preventDefaultForScrollKeys(e) {
	if (keys[e.keyCode]) {
		this.preventDefault(e);
		return false;
	}
};

export function disableScroll() {
	if (window.addEventListener) {
		window.addEventListener("DOMMouseScroll", this.preventDefault, false);
	}
	window.onwheel = this.preventDefault;
	window.onmousewheel = document.onmousewheel = this.preventDefault;
	window.ontouchmove = this.preventDefault;
	document.onkeydown = this.preventDefaultForScrollKeys.bind(this);
};

export function enableScroll() {
	if (window.removeEventListener) {
		window.removeEventListener(
			"DOMMouseScroll",
			this.preventDefault,
			false
		);
	}
	window.onmousewheel = document.onmousewheel = null;
	window.onwheel = null;
	window.ontouchmove = null;
	document.onkeydown = null;
};

export function validateForm(form, rules) {
    let errors = Object
        .keys(form)
        .map(key => [key, form[key]])
        .filter(field => rules.required.includes(field[0]) && !field[1])
        .reduce((errors, field) => {
            return {...errors, [field[0]]: "This field is required"}
        }, {})

    if (rules.email && !errors.hasOwnProperty(rules.email) && !this.validateEmail(form.email)) {
        errors[rules.email] = "Invalid email"
    }

    return errors;
}

export function isRequestSuccessful(statusCode) {
	return [200, 201, 202, 203, 204, 205, 206, 207, 208, 226].includes(statusCode)
}

export function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email.toLowerCase());
}

export function isObjEmpty(obj) {
	return Object.keys(obj).length === 0 && obj.constructor === Object
}

export function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

export function dataURItoBlob(dataURI) {
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
        byteString = atob(dataURI.split(',')[1]);

	} else {
        byteString = unescape(dataURI.split(',')[1]);
	}

    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}