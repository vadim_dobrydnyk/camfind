import * as Helper from './helper.js'

import SectionUploadsModule from './SectionUploadsModule.js'

document.addEventListener("DOMContentLoaded", function() {
	var camfind = new CamfindModule();
	camfind.init();
});

function CamfindModule() {
	var self = this;
	this.apiHost = "https://api.camfindapp.com";
	this.errorClass = "form__control--error";
	this.isHeaderFixed = false;
	
	this.isHomepage = window.document.body.classList.contains("js-homepage");
	this.isContactUs = window.document.body.classList.contains("js-contact");
	this.isCloudSight = window.document.body.classList.contains("js-cloud-sight");

	this.DOM = {};
	this.controller = {};
	this.myTyped = {};
	this.myScrollMagic = {};

	this.pointerDown = false;
	this.currentSlideIndex = 0;
	this.followingSlideIndex = 0;
	this.drag = {
		startX: 0,
		endX: 0,
		startY: 0,
		letItGo: null
	};

	this.DOM.contactUs = {};
	this.DOM.pointList = null;

	this.DOM.sectionOne = document.querySelector(".section--one");
	this.DOM.sectoinTwo = document.querySelector(".section--two");

	this.DOM.subscribeBtns = document.querySelectorAll(".subscribe-js")

	this.DOM.circle = document.querySelector(
		".cat__circle:not(.cat__circle--fill)"
	);
	this.DOM.fillCircle = document.querySelector(".cat__circle--fill circle");

	this.DOM.slider = document.querySelector(".slider__container");
	this.DOM.sliderItems = document.querySelectorAll(
		".slider__container .slider__item"
	);
	this.DOM.sliderPrev = document.querySelector(".slider__control--prev");
	this.DOM.sliderNext = document.querySelector(".slider__control--next");

	this.DOM.gif = document.querySelector(".cat .cat__gif");
	this.DOM.gifBackground = document.querySelector(
		".cat .cat__gif-background"
	);

	this.DOM.animatedText = document.querySelector(".animated-text");
	this.DOM.header = document.querySelector(".header");
	this.DOM.subscribeFrom = document.querySelector(".js-subscribe-from");
	this.DOM.subscribeFromInput = document.querySelector(".js-subscribe-from-input");
	this.DOM.btnBlock = document.querySelector(".js-btn-block");
	this.DOM.googleBtn = document.querySelector(".js-google-btn");
	this.DOM.closeBtn = document.querySelector(".js-close-btn");

	this.DOM.contactSubmitBtn = document.querySelector(".submit-contact-js");
	this.DOM.ModalSuccess = document.querySelector(".contact-modal-success-js");
	this.DOM.ModalFail = document.querySelector(".contact-modal-fail-js");
	this.DOM.contactContainer = document.querySelector(
		".contact-container-js"
	);

	this.DOM.subscribeFromFooter = document.querySelector(
		".js-subscribe-from-footer"
	);
	this.DOM.subscribeFromFooterInput = document.querySelector(
		".js-subscribe-from-footer-input"
	);
	this.DOM.btnBlockFooter = document.querySelector(".js-btn-block-footer");
	this.DOM.googleBtnFooter = document.querySelector(".js-google-btn-footer");

	this.DOM.nav = document.querySelector(".nav");
	this.DOM.navHamburger = document.querySelector(".nav__hamburger");

	this.DOM.accordionItems = document.querySelectorAll(".accordion__item");
	this.DOM.activeAccordionItem = document.querySelector(
		".accordion__item--active"
	);

	this.DOM.corporationsActive = document.querySelector(
		".corporations-list__item--active"
	);
	this.DOM.corporations = document.querySelectorAll(
		".corporations-list__item"
	);

	this.DOM.hideModalSuccessBtns = document.querySelectorAll(".contact-modal-success-js .hide-modal-js");
	this.DOM.hideModalFailBtns = document.querySelectorAll(".contact-modal-fail-js .hide-modal-js");

	this.DOM.navHamburger.addEventListener(
		"click",
		function(e) {
			self.toggleMenu(e);
		},
		false
	);

	if (this.DOM.contactSubmitBtn) {

		this.DOM.contactUs.name = document.querySelector(".form-name-js");
		this.DOM.contactUs.email = document.querySelector(".form-email-js");
		this.DOM.contactUs.message = document.querySelector(".form-message-js");

		this.DOM.contactUs.name.onclick = function(event) { self.clearError(event, self) };
		this.DOM.contactUs.email.onclick = function(event) { self.clearError(event, self) };
		this.DOM.contactUs.message.onclick = function(event) { self.clearError(event, self) };

		this.DOM.contactSubmitBtn.addEventListener("click", function(e) {
			self.sendContactUsForm();
		});
		
	}

	if (this.DOM.subscribeBtns && this.DOM.subscribeBtns instanceof NodeList) {
		Array.prototype.map.call(this.DOM.subscribeBtns, function(item) {
			item.addEventListener("click", function(e) {
				let input;
				const type = e.currentTarget.getAttribute("data-subscribe");

				if (type == "footer") {
					input = self.DOM.subscribeFromFooterInput
				} else if (type == "hero") {
					input = self.DOM.subscribeFromInput
				}

				self.sendSubscribeEmail(input);
			});
		});

		["subscribeFromFooterInput", "subscribeFromInput"].map(inputName => {


			if (!this.DOM[inputName]) return;

			this.DOM[inputName].addEventListener(
				"click",
				() => {
					this.DOM[inputName].classList.remove("email-input--error")
				},
				false
			)
		})

	}

	if (this.DOM.hideModalSuccessBtns.length && this.DOM.hideModalSuccessBtns instanceof NodeList) {
		Array.prototype.map.call(this.DOM.hideModalSuccessBtns, function(item) {
			item.addEventListener("click", function(e) {
				self.closeModal("Success");
			});
		});
	}

	if (this.DOM.hideModalFailBtns.length && this.DOM.hideModalFailBtns instanceof NodeList) {
		Array.prototype.map.call(this.DOM.hideModalFailBtns, function(item) {
			item.addEventListener("click", function(e) {
				self.closeModal("Fail");
			});
		});
	}

	this.DOM.googleBtn &&
		this.DOM.googleBtn.addEventListener("click", function(e) {
			self.showSubscribeForm();
		});
	this.DOM.googleBtnFooter &&
		this.DOM.googleBtnFooter.addEventListener("click", function(e) {
			self.showSubscribeFormFooter();
		});
	this.DOM.closeBtn &&
		this.DOM.closeBtn.addEventListener("click", function(e) {
			self.closeSubscribeForm();
		});
}

CamfindModule.prototype.clearError = function(event, self) {
	event.currentTarget.parentNode.classList.remove(self.errorClass)
}

CamfindModule.prototype.showError = function(field, errorText) {
	this.DOM.contactUs[field].parentNode.classList.add(this.errorClass);
	this.DOM.contactUs[field].parentNode.querySelector(".error-text-js").innerHTML = errorText;
}

CamfindModule.prototype.clearForm = function () {
	this.DOM.contactUs.name.value = "";
	this.DOM.contactUs.email.value = "";
	this.DOM.contactUs.message.value = "";
}

CamfindModule.prototype.sendContactUsForm = function() {

	var rules = {
        required: ["name", "email", "message"],
        email: "email"
	}

	var body = {
		"email": {
			"email": this.DOM.contactUs.email.value,
			"name": this.DOM.contactUs.name.value,
			"message": this.DOM.contactUs.message.value
		}
	}

	var errors = Helper.validateForm(body.email, rules);
	if (Helper.isObjEmpty(errors)) {

	} else {
		for(var error in errors) { 
			if (errors.hasOwnProperty(error)) {
				this.showError(error, errors[error]);
			}
		}
	}

	if (Helper.isObjEmpty(errors)) {
		var xhr = new XMLHttpRequest();
		xhr.open("POST",  this.apiHost + "/v1/contact_us", true);
		xhr.setRequestHeader('Content-type', 'application/json');
		xhr.onreadystatechange = function(self) {
			if (Helper.isRequestSuccessful(xhr.status)) {
				this.showModal("Success")
				this.clearForm()
			} else {
				this.showModal("Fail")
			}
		}.bind(this);
   		xhr.send(JSON.stringify(body));
	}

}

CamfindModule.prototype.sendSubscribeEmail = function(input) {

	var rules = {
        required: ["email"],
        email: "email"
	}

	var body = {
		"subscriber": {
			"email": input.value
		}
	}

	var errors = Helper.validateForm(body.subscriber, rules);

	if (Helper.isObjEmpty(errors)) {
		var xhr = new XMLHttpRequest();
		xhr.open("POST",  this.apiHost + "/v1/subscribers", true);
		xhr.setRequestHeader('Content-type', 'application/json');
		xhr.onreadystatechange = function(self) {
			if (Helper.isRequestSuccessful(xhr.status)) {
				this.showModal("Success")
			} else {
				this.showModal("Fail")
			}

			if (this.DOM.subscribeFromInput) {
				this.DOM.subscribeFromInput.value = "";
			}
			if (this.DOM.subscribeFromFooterInput) {
				this.DOM.subscribeFromFooterInput.value = "";
			}
		}.bind(this);
		xhr.send(JSON.stringify(body));
	} else {
		input.classList.add('email-input--error');
	}
}



CamfindModule.prototype.getCorporationByIndex = function(index) {
	if (index !== 0 && !index) {
		return this.DOM.corporationsActive;
	}

	for (var i = this.DOM.corporations.length - 1; i >= 0; i--) {
		if (
			+this.DOM.corporations[i].getAttribute("data-slider-index") ===
			index
		) {
			return this.DOM.corporations[i];
		}
	}
};

CamfindModule.prototype.setActiveCorporation = function(currentSlide) {
	this.DOM.corporationsActive.classList.remove(
		"corporations-list__item--active"
	);
	var currentSlide = this.getCorporationByIndex(currentSlide);
	currentSlide.classList.add("corporations-list__item--active");
	this.DOM.corporationsActive = currentSlide;
};

CamfindModule.prototype.setSlidersEvents = function() {
	this.DOM.slider.addEventListener(
		"touchstart",
		this.touchstartHandler.bind(this)
	);
	this.DOM.slider.addEventListener(
		"touchend",
		this.touchendHandler.bind(this)
	);
	this.DOM.slider.addEventListener(
		"touchmove",
		this.touchmoveHandler.bind(this)
	);

	this.DOM.sliderPrev.addEventListener(
		"click",
		this.prevSlide.bind(this),
		false
	);

	this.DOM.sliderNext.addEventListener(
		"click",
		this.nextSlide.bind(this),
		false
	);

	for (var i = 0; i < this.DOM.corporations.length; i++) {
		this.DOM.corporations[i].setAttribute("data-slider-index", i);
		this.DOM.corporations[i].addEventListener(
			"click",
			function(e) {
				this.DOM.corporationsActive.classList.remove(
					"corporations-list__item--active"
				);
				e.currentTarget.classList.add(
					"corporations-list__item--active"
				);
				this.DOM.corporationsActive = e.currentTarget;

				this.setActiveSlide(
					+e.currentTarget.getAttribute("data-slider-index")
				);
				this.setSliderDirection();
			}.bind(this)
		);
	}
};

CamfindModule.prototype.toggleSliderDirection = function() {
	for (var i = 0; i < this.DOM.sliderItems.length; i++) {
		if (
			this.DOM.sliderItems[i].classList.contains("slider__item--right") ||
			this.DOM.sliderItems[i].classList.contains("slider__item--left")
		) {

			var isRight = this.DOM.sliderItems[i].classList.contains(
				"slider__item--right"
			);

			this.DOM.sliderItems[i].classList.remove(
				isRight ? "slider__item--right" : "slider__item--left"
			);
			this.DOM.sliderItems[i].classList.add(
				!isRight ? "slider__item--right" : "slider__item--left"
			);
		}
	}
};

CamfindModule.prototype.setSliderDirection = function() {
	for (let i = 0; i < this.DOM.sliderItems.length; i++) {
		if (
			this.DOM.sliderItems[i].classList.contains("slider__item--right") ||
			this.DOM.sliderItems[i].classList.contains("slider__item--left")
		) {
			this.DOM.sliderItems[i].classList.remove("slider__item--right");
			this.DOM.sliderItems[i].classList.remove("slider__item--left");

			if (i !== this.currentSlideIndex) {
				const newClass = i < this.currentSlideIndex
					? "slider__item--left"
					: "slider__item--right"
				this.DOM.sliderItems[i].classList.add(newClass);
			}
		}
	}
};

CamfindModule.prototype.setActiveSlide = function(newIndex) {
	if (newIndex === this.currentSlideIndex) return;

	this.DOM.sliderItems[newIndex].classList.remove(
		"slider__item--left",
		"slider__item--right"
	);
	var sliderClass =
		newIndex > this.currentSlideIndex
			? "slider__item--left"
			: "slider__item--right";
	this.DOM.sliderItems[this.currentSlideIndex].classList.add(sliderClass);

	this.currentSlideIndex = newIndex;
};

CamfindModule.prototype.touchstartHandler = function(e) {
	e.stopPropagation();
	this.pointerDown = true;
	this.drag.startX = e.touches[0].pageX;
	this.drag.startY = e.touches[0].pageY;
};

CamfindModule.prototype.touchendHandler = function(e) {
	e.stopPropagation();
	this.pointerDown = false;
	this.DOM.sliderItems[this.currentSlideIndex].style.transition = "";
	this.DOM.sliderItems[this.followingSlideIndex].style.transition = "";
	if (this.drag.endX) {
		this.updateAfterDrag();
	}
	this.clearDrag();
};

CamfindModule.prototype.touchmoveHandler = function(e) {
	e.stopPropagation();

	if (this.drag.letItGo === null) {
		this.drag.letItGo =
			Math.abs(this.drag.startY - e.touches[0].pageY) <
			Math.abs(this.drag.startX - e.touches[0].pageX);
	}

	if (this.pointerDown && this.drag.letItGo) {
		e.preventDefault();
		this.drag.endX = e.touches[0].pageX;
		this.DOM.sliderItems[this.currentSlideIndex].style.webkitTransition =
			"all 0ms ease-in-out";
		this.DOM.sliderItems[this.currentSlideIndex].style.transition =
			"all 0ms ease-in-out";

		var width = this.DOM.slider.offsetWidth * 0.85;
		var transition = (this.drag.startX - this.drag.endX) * -1;
		var persentTransition = 100 * transition / width;

		var direction = this.drag.endX - this.drag.startX > 0;
		var transitionDirection = direction ? -100 : 100;

		var isEdge =
			(this.currentSlideIndex === 0 && direction) ||
			(this.currentSlideIndex === this.DOM.sliderItems.length - 1 &&
				!direction);

		this.followingSlideIndex = isEdge
			? this.currentSlideIndex
			: direction
				? this.currentSlideIndex - 1
				: this.currentSlideIndex + 1;

		this.DOM.sliderItems[this.currentSlideIndex].style["transform"] =
			"translateX(" + persentTransition + "%)";

		if (!isEdge) {
			this.DOM.sliderItems[this.followingSlideIndex].style["transition"] =
				"all 0ms";
			this.DOM.sliderItems[this.followingSlideIndex].style["transform"] =
				"translateX(" +
				(persentTransition + transitionDirection) +
				"%)";
			this.DOM.sliderItems[this.followingSlideIndex].style["opacity"] =
				"1";
		}
	}
};

CamfindModule.prototype.clearDrag = function() {
	this.drag = {
		startX: 0,
		endX: 0,
		startY: 0,
		letItGo: null
	};
};

CamfindModule.prototype.updateAfterDrag = function() {
	var movement = this.drag.endX - this.drag.startX;
	var movementDistance = Math.abs(movement);

	this.DOM.sliderItems[this.currentSlideIndex].style.transform = "";
	this.DOM.sliderItems[this.followingSlideIndex].style.transform = "";

	if (this.currentSlideIndex === this.followingSlideIndex) {
		return false;
	}

	if (movement > 0 && movementDistance > 200) {
		this.prevSlide();
	} else if (movement < 0 && movementDistance > 200) {
		this.nextSlide();
	}
};

CamfindModule.prototype.nextSlide = function(e) {
	var lastIndex = this.DOM.sliderItems.length - 1;
	var nextIndex =
		this.currentSlideIndex === lastIndex ? 0 : this.currentSlideIndex + 1;
	this.DOM.sliderItems[nextIndex].classList.remove("slider__item--right");
	this.DOM.sliderItems[nextIndex].classList.remove("slider__item--left");

	this.DOM.sliderItems[this.currentSlideIndex].classList.add(
		"slider__item--left"
	);
	if (nextIndex < this.currentSlideIndex) {
		this.toggleSliderDirection();
	}

	this.currentSlideIndex =
		this.currentSlideIndex === lastIndex ? 0 : nextIndex;
	this.setActiveCorporation(this.currentSlideIndex);
};

CamfindModule.prototype.prevSlide = function(e) {
	var lastIndex = this.DOM.sliderItems.length - 1;
	var prevIndex =
		this.currentSlideIndex === 0 ? lastIndex : this.currentSlideIndex - 1;
	this.DOM.sliderItems[prevIndex].classList.remove("slider__item--left");
	this.DOM.sliderItems[prevIndex].classList.remove("slider__item--right");
	
	this.DOM.sliderItems[this.currentSlideIndex].classList.add(
		"slider__item--right"
	);

	if (prevIndex > this.currentSlideIndex) {
		this.toggleSliderDirection();
	}

	this.currentSlideIndex =
		this.currentSlideIndex === 0 ? lastIndex : prevIndex;
	this.setActiveCorporation(this.currentSlideIndex);
};

CamfindModule.prototype.setAccordionEvents = function() {
	for (var i = 0; i < this.DOM.accordionItems.length; i++) {
		var accordionItem = this.DOM.accordionItems[i];
		accordionItem.lastElementChild.setAttribute(
			"data-accordion-h",
			accordionItem.offsetHeight + "px"
		);
		accordionItem.setAttribute("data-index", i);
		accordionItem.lastElementChild.style.maxHeight = "0px";

		accordionItem.addEventListener(
			"click",
			function(i) {
				this.cat.isDone &&
					this.setActiveAccordionItem(this.DOM.accordionItems[i]);
			}.bind(this, i)
		);
	}
	this.DOM.accordionItems[0].lastElementChild.style[
		"maxHeight"
	] = this.DOM.accordionItems[0].lastElementChild.getAttribute(
		"data-accordion-h"
	);
};

CamfindModule.prototype.setActiveAccordionItem = function(newActiveItem) {
	if (newActiveItem == this.DOM.activeAccordionItem) {
		return false;
	}

	newActiveItem.classList.add("accordion__item--active");
	newActiveItem.lastElementChild.style.maxHeight = newActiveItem.lastElementChild.getAttribute(
		"data-accordion-h"
	);
	this.DOM.activeAccordionItem.lastElementChild.style.maxHeight = "0";
	this.DOM.activeAccordionItem.classList.remove("accordion__item--active");
	this.DOM.activeAccordionItem = newActiveItem;
};

CamfindModule.prototype.restartGif = function(gif) {
	var tempSrc = gif.getAttribute("src");
	gif.setAttribute("src", "");
	setTimeout(function() {
		gif.src = tempSrc;
	}, 0);
};

CamfindModule.prototype.toggleMenu = function(e) {
	this.DOM.nav.classList.contains("nav--opened")
		? this.closeMenu()
		: this.openMenu();
};

CamfindModule.prototype.openMenu = function() {
	this.DOM.nav.classList.add("nav--opened");
	Helper.disableScroll();
};

CamfindModule.prototype.closeMenu = function() {
	this.DOM.nav.classList.remove("nav--opened");
	Helper.enableScroll();
};

CamfindModule.prototype.setAnimation = function() {
	if (window.pageYOffset < 80) {
		var header = TweenMax.fromTo(
			".header",
			0.5,
			{ y: "-50%", autoAlpha: 0 },
			{
				y: "0%",
				autoAlpha: 1,
				onComplete: () => {
					this.DOM.header.setAttribute('style', '')
				}
			}
		);
	}
	

	var sectionOneIphons = TweenMax.fromTo(
		".section--one .container__half:nth-child(2)",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0%", autoAlpha: 1, onComplete:function() { document.querySelector(".section--one .container__half:nth-child(2)").style.transform = "";  } }
	);
	var sectionOneAnimatedText = TweenMax.fromTo(
		".animated-text-container",
		1,
		{ autoAlpha: 0 },
		{ autoAlpha: 1, delay: 1 }
	);
	var sectionOneMainText = TweenMax.fromTo(
		".section--one .main-text",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0", autoAlpha: 1, delay: 0.3 }
	);
	var sectioOneText = TweenMax.fromTo(
		".container__content .text",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0", autoAlpha: 1, delay: 0.4 }
	);
	var sectionOneBtnGroup = TweenMax.fromTo(
		".container__content .buttons-groups",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0", autoAlpha: 1, delay: 0.5 }
	);

	var sectionSecondContent = TweenMax.fromTo(
		".section--two .container",
		1,
		{ autoAlpha: 0 },
		{ autoAlpha: 1 }
	);
	var secondSectionScene = new ScrollMagic.Scene({
		triggerElement: ".section--two",
		triggerHook: 0.3,
		offset: 0,
		reverse: false
	});

	var sectionThreeTitle = TweenMax.fromTo(
		".section--three .text--bold",
		1,
		{ y: "90%", autoAlpha: 0 },
		{ y: "0%", autoAlpha: 1 }
	);
	var sectionThreeFeature = TweenMax.fromTo(
		".feature",
		1,
		{ y: "20%", autoAlpha: 0 },
		{ y: "0%", delay: 0.2, autoAlpha: 1 }
	);
	var threeSectionScene = new ScrollMagic.Scene({
		triggerElement: ".section--three",
		triggerHook: 0.9,
		offset: 0,
		reverse: false
	});

	var sectionFourContent = TweenMax.fromTo(
		".section--four .container",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0", autoAlpha: 1 }
	);
	var fourSectionScene = new ScrollMagic.Scene({
		triggerElement: ".section--four",
		triggerHook: 0.2,
		offset: 0,
		reverse: false
	});

	var sectionFiveContent = TweenMax.fromTo(
		".section--five .container",
		1,
		{ y: "100px", autoAlpha: 0 },
		{ y: "0", autoAlpha: 1 }
	);
	var fiveSectionScene = new ScrollMagic.Scene({
		triggerElement: ".section--five",
		triggerHook: 0.5,
		offset: 0,
		reverse: false
	});

	secondSectionScene = secondSectionScene
		.setTween([sectionSecondContent])
		.addTo(this.controller)
		.on(
			"enter",
			function() {
				this.cat.isDone = false;
			}.bind(this)
		);

	threeSectionScene = threeSectionScene
		.setTween([sectionThreeTitle, sectionThreeFeature])
		.addTo(this.controller);

	fourSectionScene = fourSectionScene
		.setTween([sectionFourContent])
		.addTo(this.controller);

	fiveSectionScene = fiveSectionScene
		.setTween([sectionFiveContent])
		.addTo(this.controller);
};

CamfindModule.prototype.initCatAnim = function() {
	return new TimelineMax()
		.set(this.DOM.fillCircle, { strokeDashoffset: "1855" })
		.to(this.DOM.fillCircle, 0, {
			strokeDashoffset: "1855",
			ease: Power0.easeNone,
			onStart: function() {
				this.restartGif(this.DOM.gif);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 3, {
			strokeDashoffset: "2250",
			ease: Power0.easeNone,
			onStart: function() {
				this.cat.isStarted = true;
				if (!this.cat.firstTimeDone) {
					this.DOM.gif.classList.add("cat__gif--active");
					this.DOM.gifBackground.style.opacity = 0;
				}
			}.bind(this),
			onComplete: function() {
				if (!this.cat.firstTimeDone) {
					this.setActiveAccordionItem(this.DOM.accordionItems[1]);
					this.DOM.gif.classList.add("cat__gif--active");
				}
				this.DOM.pointList[1].classList.add(
					"point-list__point--active"
				);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 3, {
			strokeDashoffset: "2660",
			ease: Power0.easeNone,
			onReverseComplete: function() {
				if (!this.cat.firstTimeDone) {
					this.setActiveAccordionItem(this.DOM.accordionItems[0]);
				}
				this.DOM.pointList[1].classList.remove(
					"point-list__point--active"
				);
			}.bind(this),
			onComplete: function() {
				if (!this.cat.firstTimeDone) {
					this.setActiveAccordionItem(this.DOM.accordionItems[2]);
				}
				this.DOM.pointList[2].classList.add(
					"point-list__point--active"
				);
				this.DOM.pointList[3].classList.remove(
					"point-list__point--active"
				);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 3, {
			strokeDashoffset: "3050",
			ease: Power0.easeNone,
			onReverseComplete: function() {
				if (!this.cat.firstTimeDone) {
					this.setActiveAccordionItem(this.DOM.accordionItems[1]);
				}
				this.DOM.pointList[2].classList.remove(
					"point-list__point--active"
				);
			}.bind(this),
			onComplete: function() {
				this.DOM.pointList[3].classList.add(
					"point-list__point--active"
				);
				this.cat.isDone = true;
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 2, {
			strokeDashoffset: "3050",
			onComplete: function() {
				this.DOM.pointList[3].classList.remove(
					"point-list__point--active"
				);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 1, {
			strokeDashoffset: "2660",
			ease: Power0.easeNone,
			onComplete: function() {
				this.DOM.pointList[2].classList.remove(
					"point-list__point--active"
				);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 1, {
			strokeDashoffset: "2250",
			ease: Power0.easeNone,
			onComplete: function() {
				this.DOM.pointList[1].classList.remove(
					"point-list__point--active"
				);
			}.bind(this)
		})
		.to(this.DOM.fillCircle, 1, {
			strokeDashoffset: "1855",
			ease: Power0.easeNone,
			onComplete: function() {
				this.DOM.pointList[1].classList.remove(
					"point-list__point--active"
				);
				this.catInCircleAnimation.restart();
				this.cat.firstTimeDone = true;
			}.bind(this)
		});
};

CamfindModule.prototype.showSubscribeForm = function() {
	if (window.innerWidth < 570) {
		return;
	}
	this.DOM.subscribeFrom.style.display = "flex";
	this.DOM.btnBlock.style.display = "none";
};

CamfindModule.prototype.showSubscribeFormFooter = function() {
	if (window.innerWidth < 570) {
		return;
	}
	this.DOM.subscribeFromFooter.style.display = "flex";
	this.DOM.btnBlockFooter.style.display = "none";
};

CamfindModule.prototype.closeSubscribeForm = function() {
	this.DOM.subscribeFrom.style.display = "none";
	this.DOM.btnBlock.style.display = "flex";
};

CamfindModule.prototype.animationOffset = function() {
	return (
		(window.innerHeight -
			document.querySelector(".section--two").offsetHeight) /
			2 -
		140
	);
};

CamfindModule.prototype.showModal = function(type) {
	Helper.disableScroll();

	this.DOM.contactContainer.classList.add("modal-container--active");
	setTimeout(
		function(type) {
			this.DOM['Modal' + type].classList.add("modal--active");
		}.bind(this, type),
		0
	);
};

CamfindModule.prototype.closeModal = function(type) {
	Helper.enableScroll();
	this.DOM.contactContainer.classList.remove("modal-container--active");
	this.DOM['Modal' + type].classList.remove("modal--active");
};

CamfindModule.prototype.initCatCircle = function() {
	this.cat = {
		offset: this.animationOffset(),
		firstTimeDone: false,
		isStarted: false,
		isDone: false,
		circumference: 2 * Math.PI * 260
	};

	this.DOM.fillCircle.style.strokeDasharray = this.cat.circumference;
	this.DOM.fillCircle.setAttribute("stroke-dashoffset", 1855);

	var points = document.querySelectorAll(".point-list__point[data-order]");
	points = Array.prototype.slice.call(points, 0).sort(function(a, b) {
		return a.getAttribute("data-order") - b.getAttribute("data-order");
	});

	this.DOM.pointList = points;

	this.scene = new ScrollMagic.Scene({
		triggerHook: 0,
		offset: -this.cat.offset,
		triggerElement: ".section--two"
	})
		.addTo(this.controller)
		.on(
			"enter",
			function() {
				if (!this.cat.isStarted) {
					this.catInCircleAnimation = this.initCatAnim();
				}
			}.bind(this)
		);
};

CamfindModule.prototype.initTypedAnimation = function() {
	this.myTyped = new Typed(this.DOM.animatedText, {
		startDelay: 100,
		strings: ["Smart", "E^100asy", "Understandable", "Fast"],
		typeSpeed: 150,
		loop: true,
	});
};

CamfindModule.prototype.initFixedHeader = function(callback) {
	document.addEventListener(
		"scroll",
		event => {
			callback.call(null, this)
		},
		false
	)
}

CamfindModule.prototype.scrollCallbackDefault = (self) => {
	if (window.pageYOffset > 0 && !self.isHeaderFixed) {
		self.isHeaderFixed = true;
		self.DOM.header.classList.add("header--fixed")
	} else if (window.pageYOffset <= 0 && self.isHeaderFixed) {
		self.isHeaderFixed = false;
		self.DOM.header.classList.remove("header--fixed")
	}
}

CamfindModule.prototype.scrollCallbackIndexpage = (self) => {
	var rect = self.DOM.sectionOne.getClientRects()[0].bottom;

	if (rect < 0 && !self.isHeaderFixed) {
		self.isHeaderFixed = true;
		self.DOM.header.classList.add("header--fixed")
		setTimeout(() => {
			self.DOM.header.classList.add("header--active")
		}, 300);

	} else if (rect > 0 && self.isHeaderFixed) {
		self.isHeaderFixed = false;
		self.DOM.header.classList.add("header--close")
		setTimeout(() => {
		 	self.DOM.header.classList.remove("header--active", "header--fixed", "header--close")
		}, 200);
	}
}

CamfindModule.prototype.init = function() {
	if (this.isHomepage) {
		this.controller = new ScrollMagic.Controller();
		this.initCatCircle();
		setTimeout(this.initTypedAnimation.bind(this), 1400);
		this.setSlidersEvents();
		this.setAccordionEvents();
		this.setAnimation();
	} 
	
	this.initFixedHeader(this.scrollCallbackDefault);

	if (this.isCloudSight) {
		this.sectionUploadsModule = new SectionUploadsModule()
		this.sectionUploadsModule.init(this);
	}
};
