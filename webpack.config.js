var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var BrowserSyncPlugin = require("browser-sync-webpack-plugin");

module.exports = {
    entry: [
        "core-js/es7/array",
        "./assets/scss/main.scss",
        "./assets/js/main.js"
    ],
    output: {
        filename: "./assets/js/main.min.js"
    },
    module: {
        rules: [
            {
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract([{ loader: 'css-loader', options: { minimize: true }}, "sass-loader"])
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "assets/css/style.css"
        }),
        new BrowserSyncPlugin({
            host: "localhost",
            port: 3002,
            server: { baseDir: [""] },
            reload: false
        })
    ]
};
